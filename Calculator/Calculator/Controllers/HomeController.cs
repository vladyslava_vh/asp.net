﻿using Calculator.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Calculator.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Calc(string number1, string number2, string btn)
        {
            double result = default;
            switch (btn)
            {
                case "+":
                    result = double.Parse(number1) + double.Parse(number2);
                    break;
                case "-":
                    result = double.Parse(number1) - double.Parse(number2);
                    break;
                case "*":
                    result = double.Parse(number1) * double.Parse(number2);
                    break;
                case "/":
                    if (double.Parse(number2) != 0)
                        result = double.Parse(number1) / double.Parse(number2);
                    else
                    {
                        ViewData["Result"] = "ERROR: You can't divide by zero";
                        return View();
                    }
                    break;
            }

            ViewData["Result"] = $"{number1} {btn} {number2} = {result}";
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

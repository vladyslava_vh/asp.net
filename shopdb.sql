IF DB_ID('Shop') IS NOT NULL
BEGIN
	USE master;
	ALTER DATABASE Shop SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE Shop;
END
GO

CREATE DATABASE Shop;
GO

USE Shop
GO

CREATE TABLE Categories
(
    Id int NOT NULL IDENTITY PRIMARY KEY,
    [Name] nvarchar(30) NOT NULL CHECK ([Name] <> N'' AND [Name] LIKE '%')
);
GO

CREATE TABLE Manufacturers
(
    Id int NOT NULL IDENTITY PRIMARY KEY,
    [Name] nvarchar(15) NOT NULL CHECK([Name] <> N'' AND [Name] LIKE '%')
); 
GO

CREATE TABLE Products (
	Id int NOT NULL IDENTITY PRIMARY KEY,
	[Name] nvarchar(MAX) NOT NULL CHECK ([Name] <> N'' AND [Name] LIKE '%'),	
	Color nvarchar(20) NOT NULL CHECK (Color <> N'' AND Color LIKE '%'),
	CategoryFK int NOT NULL FOREIGN KEY REFERENCES Categories(Id) ON DELETE CASCADE ON UPDATE CASCADE,
	ManufacturerFK int NOT NULL FOREIGN KEY REFERENCES Manufacturers(Id) ON DELETE NO ACTION ON UPDATE CASCADE,
	[Description] nvarchar(MAX) NULL,
	[Count] int NOT NULL,
	Price money NOT NULL
);
GO

CREATE TABLE Images (
	Id int NOT NULL IDENTITY PRIMARY KEY,
	[Url] nvarchar(100) NOT NULL CHECK ([Url] <> N'') UNIQUE,
	ProductFK int NOT NULL FOREIGN KEY REFERENCES Products(Id) ON DELETE CASCADE ON UPDATE CASCADE
);
GO

CREATE TABLE Buyers
(
    Id int NOT NULL IDENTITY PRIMARY KEY,
    FirstName nvarchar(20) NOT NULL CHECK (FirstName <> N'' AND FirstName LIKE '%'),
	LastName nvarchar(20) NOT NULL CHECK (LastName <> N'' AND LastName LIKE '%'),
	Phone nvarchar(13) NOT NULL CHECK (Phone LIKE '+380[1-9][1-9][1-9][1-9][1-9][1-9][1-9][1-9][1-9]'),
	Email nvarchar(30) NULL CHECK (Email <> N'' AND Email LIKE '%@%.%'),
	Birthday date NULL CHECK (Birthday <> N'' AND YEAR(Birthday) <= YEAR(GETDATE()) - 18)
);
GO

CREATE TABLE Orders
(
    BuyerFK int NOT NULL FOREIGN KEY REFERENCES Buyers(Id) ON DELETE NO ACTION ON UPDATE CASCADE,
    ProductFK int NOT NULL FOREIGN KEY REFERENCES Products(Id) ON DELETE CASCADE ON UPDATE CASCADE,
	[Count] int NOT NULL CHECK ([Count] >= 1),
	[DateTime] datetime2 NOT NULL CHECK ([DateTime] <= GETDATE()),
	PRIMARY KEY(BuyerFK, ProductFK)
);
GO

--����������
INSERT INTO Categories([Name])
VALUES('Laptops and computers'), ('Smartphones'), ('TV'), ('Electronics')

INSERT INTO Manufacturers([Name])
VALUES('Apple'), ('Samsung')

INSERT INTO Products([Name], Color, CategoryFK, ManufacturerFK, [Description], [Count], Price)
VALUES('Apple iPhone 11 64GB Slim Box (MHDG3)', 'green', 2, 1, 
	   '����� (6.1", IPS (Liquid Retina HD), 1792x828)/ Apple A13 Bionic/ �������� ������� ������: 12 �� + 12 ��, ����������� ������: 12 ��/ RAM 4 ��/ 64 �� ���������� ������/ 3G/ LTE/ GPS/ �������/ Nano-SIM/ iOS 13 / 3046 ��*�

�������� ������ � ���������� �� �������������.', 5, 19999),
	  ('Apple iPhone 12 mini 64GB', 'purple', 2, 1,
	  '����� (5.4", OLED (Super Retina XDR), 2340�1080) / Apple A14 Bionic / ������� �������� ������: 12 �� + 12 ��, ����������� ������: 12 �� / 64 �� ���������� ������ / 3G / LTE / 5G / GPS / Nano-SIM, eSIM / iOS 1', 3, 22999),
	  ('Samsung Galaxy S21 8/256GB Phantom (SM-G991BZWGSEK)', 'white', 2, 2,
	  '����� (6.2", Dynamic AMOLED 2X, 2400x1080)/ Samsung Exynos 2100 (1 x 2.9 ��� + 3 x 2.8 ��� + 4 x 2.2 ���)/ ������� �������� ������: 64 �� + 12 �� + 12 ��, ����������� 10 ��/ RAM 8 ��/ 256 �� ���������� ������/ 3G/ LTE/ 5G/ GPS/ ��������� 2� SIM-���� (Nano-SIM)/ Android 11.0/ 4000 ��*�', 7, 25999),
	  ('Samsung Galaxy S20 FE (2021) 6/128GB Cloud (SM-G780GZBDSEK)', 'navy', 2, 2,
	  '����� (6.5", Super AMOLED, 2400x1080) / Qualcomm Snapdragon 865 (1�2.84 ��� + 3�2.42 ��� + 4�1.8 ���) / ������� �������� ������: 12 �� + 12 �� + 8 ��, �����������: 32 �� / RAM 6 �� / 128 �� ���������� ������ + microSD (�� 1 ��) / 3G / LTE / GPS / ��������� 2� SIM-���� (Nano-SIM) / Android 10 / 4500 ��*�', 9, 16999)

--SELECT *
--FROM Categories

--SELECT *
--FROM Manufacturers

SELECT Products.Name, Color, Categories.Name AS 'Category', Manufacturers.Name AS 'Manufacturer'
FROM Products
	 JOIN Categories ON CategoryFK = Categories.Id
	 JOIN Manufacturers ON ManufacturerFK = Manufacturers.Id